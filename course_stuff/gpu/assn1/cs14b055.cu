#include <cuda_runtime.h>
#include <stdio.h>
#include "kernels.h"

__global__ void transpose_parallel_per_row(float *in, float *out, int rows_in, int cols_in)
{
	// launched as <<<rows_in/numThreads+1, numThreads>>>
	int id = threadIdx.x + blockDim.x * blockIdx.x;
	// transpose row id
	if(id < rows_in){
        //printf("transposing row %d\n", id);
		for(int i=0; i<cols_in; i++)
			out[i*rows_in + id] = in[id*cols_in+ i];
	}
}

__global__ void 
transpose_parallel_per_element(float *in, float *out, int rows_in, int cols_in, int K1, int K2)
{
    // launched as <<<blocks,threads>>> , blocks = (rows_in/k1, cols_in/k2) threads = (k1, k2)
    
    int id_x = threadIdx.x + blockDim.x * blockIdx.x;
    int id_y = threadIdx.y + blockDim.y * blockIdx.y;
    // transpose [id_x .. id_x+k1-1][id_y .. id_y+k2-1]
    for(int i=0; i<K1; i++)
        for(int j=0; j<K2; j++){
            int x = id_x+i, y=id_y+j;
            if(x < rows_in && y < cols_in)
                out[y*rows_in + x] = in[x*cols_in + y];
        }
}
