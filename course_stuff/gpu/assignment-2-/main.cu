#include <stdio.h>
#include "kernel.h"
#include "timer.h"

int my_rand(int i){
    return (1218*i+7492)%4947;
}

void print_arr(int *arr, int n)
{
    for(int i=0; i<n; i++){
        printf("%d ",arr[i]);
    }
    printf("\n");
}

bool check_sorted(int *arr, int n){
    for(int i=1;i<n;i++){
        if(arr[i-1]>arr[i]) {
            printf("error at %d",i);
            return false;
        }
    }
    return true;
}

int cmp_less(const void* a, const void* b){
    return (*(int*)a - *(int*)b);
}
void cpusort(int *in, int *out,  int n){
    for(int i=0; i<n; i++) out[i]=in[i];
    qsort(out, n, sizeof(int), cmp_less);
}

// Generating expected output 
void transpose_CPU(float in[], float out[], int M, int N)
{
  for(int i=0; i<M; i++)
  for(int j=0; j<N; j++)
           out[j*M +i] = in[i * N + j]; 
}


int main(int argc, char** argv)
{
    const int n = 64;

    unsigned numbytes = n*sizeof(int);
    int *in = (int*) malloc(numbytes);
    int *out = (int*) malloc(numbytes);
    
    for(int i=0;i<n;i++) in[i]=my_rand(i);
    
    // CPU sort
    CPUTimer cputimer;
    cputimer.Start();
    cpusort(in, out, n);
    cputimer.Stop();
    printf("CPU sort time: %f ms\n", cputimer.Elapsed()*1000);

    // GPU sort
    int *gpu_in, *gpu_out;
    cudaMalloc(&gpu_in, numbytes);
    cudaMalloc(&gpu_out, numbytes);
    cudaMemcpy(gpu_in, in, numbytes, cudaMemcpyHostToDevice);

    GPUTimer gputimer;
    gputimer.Start();
    msort<<<2, n/2>>>(gpu_in, gpu_out, n);
    gputimer.Stop();
    printf("GPU sort time: %f ms\n", gputimer.Elapsed()*1000);

    // GPU check
    cudaMemcpy(out, gpu_in, numbytes, cudaMemcpyDeviceToHost);
    print_arr(in, n);
    print_arr(out, n);
    if(check_sorted(out, n))
        printf("Success\n");
    else
        printf("Failure\n");
}

