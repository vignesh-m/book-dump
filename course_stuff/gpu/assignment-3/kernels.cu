#include<stdio.h>
#include<cuda.h>

__global__ void histogram(int *d_input, int* d_bin, int M, int N, int BIN_COUNT)
{
    int id = blockIdx.x*blockDim.x + threadIdx.x;
    if(id < M*N){
        int val = d_input[id];
        val = val % BIN_COUNT;
        atomicInc((unsigned int*)&d_bin[val], M*N+1);
    }
}

__device__ int is_border(int idx, int M, int N){
    int iy = idx%N, ix = idx/N;
    return (ix == 0 || ix == M-1 || iy ==0 || iy == N-1);
}
__global__ void updateBC(int* d_input, int M, int N)
{
    int bidx = blockIdx.x*gridDim.y + blockIdx.y;
    int tidx = bidx*blockDim.x*blockDim.y + threadIdx.x*blockDim.y + threadIdx.y;
    if(tidx < M*N){
        if(is_border(tidx, M, N)) d_input[tidx] = 1;
    }
}
__device__ volatile int counter = 0;
__global__ void stencil(int* d_input, int M, int N)
{
    int bidx = blockIdx.x*gridDim.y + blockIdx.y;
    int b1idx = threadIdx.x*blockDim.y + threadIdx.y;
    int tidx = bidx*blockDim.x*blockDim.y + b1idx;
    int val=0;
    if(tidx < M*N && !is_border(tidx, M, N)){
        int dx[5]  = {0,0,1,0,-1}, dy[5] = {0,1,0,-1,0};
        for(int i=0; i<5; i++){
            int nidx = tidx + dx[i]*N + dy[i];
            val += d_input[nidx];
        }
    }
    int req_counter = gridDim.x*gridDim.y;
    if(b1idx == 0) {
        atomicAdd((int*)&counter, 1);
    }   
    __syncthreads();
    while(counter < req_counter) ;
    if(tidx < M*N && !is_border(tidx, M, N)){
        d_input[tidx] = (int)(0.2*val);
    }
}
