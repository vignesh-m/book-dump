#include<stdio.h>
#include<cuda.h>
#include"kernels.h"

const int m=8;
const int n=7;
const int b=16;
/*
int example[m*n] = {
    1,2,3,4,
    1,3,4,1,
    3,4,2,1,
    4,1,2,3
};
*/
int example[m*n] = {
    101, 174,  89, 121,  12, 172,  52,
     57, 212, 232,  41, 182, 128,  36,
    155, 206,   5,  75,  19, 143, 111,
    116,  54, 125, 190, 207,   8, 108,
    216,  67, 211, 105, 242,  77, 115,
    198,  90,  88, 225, 250, 175, 197,
     26, 186, 117,  53,  88, 169,  52,
    230,  31,   1,  52,  84,   7, 213
};

void print_matrix(int* arr,int m, int n){
    for(int i=0; i<m; i++){
        for(int j=0; j<n; j++)
            printf("%4d ",arr[i*n+j]);
        printf("\n");
    }   
    printf("\n");
}
int main(){
    //printf("Histogram\n");
    //print_matrix(example, m, n);

    int *d_arr, *d_bin;
    int *c_arr, *c_bin;
    c_arr = (int*)malloc(m*n*sizeof(int));
    cudaMalloc(&d_arr, m*n*sizeof(int));
    cudaMemcpy(d_arr, example, m*n*sizeof(int), cudaMemcpyHostToDevice);

    c_bin = (int*)malloc(b*sizeof(int));
    cudaMalloc(&d_bin, b*sizeof(int));
    for(int i=0; i<b; i++) c_bin[i]=0;
    cudaMemcpy(d_bin, c_bin, b*sizeof(int), cudaMemcpyHostToDevice);

    histogram<<<1,m*n>>>(d_arr,d_bin,m,n,b);

    cudaMemcpy(c_bin, d_bin, b*sizeof(int), cudaMemcpyDeviceToHost);
    //print_matrix(c_bin, 1,b);

    cudaMemcpy(d_arr, example, m*n*sizeof(int), cudaMemcpyHostToDevice);
    //dim3 blocks(2,1);
    //dim3 threads(m/2,n);
    dim3 blocks(m,n);
    dim3 threads(1,1);
    updateBC<<<blocks, threads>>>(d_arr, m, n);
    cudaMemcpy(c_arr, d_arr, m*n*sizeof(int), cudaMemcpyDeviceToHost);
    //print_matrix(c_arr, m, n);
    stencil<<<blocks, threads>>>(d_arr, m, n);
    cudaMemcpy(c_arr, d_arr, m*n*sizeof(int), cudaMemcpyDeviceToHost);
    //print_matrix(c_arr, m, n);

    
    cudaThreadSynchronize();
}
