#include <cuda.h>
#include <stdio.h>

__global__ void hello(){
    printf("hello\n");
}

int main(){
    hello<<<1,10>>>();
    cudaDeviceSynchronize();
    return 0;
}
