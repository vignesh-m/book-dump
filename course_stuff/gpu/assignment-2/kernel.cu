#include <stdio.h>
/* The kernel "msort" should sort the input array using parallel merge-sort. */

__device__ int counter;

__global__ void msort(int *d_input, int* d_temp, int n)
{
    int id = threadIdx.x + blockIdx.x*blockDim.x;
    const int INF = 1000000000;
    counter = 0;
 
    int hseg = 1;
    int seg = 2*hseg;
    while(seg<= n){
        int idx = id;
        if(idx % seg == 0){
            // sort [idx .. idx+seg-1]
            int st1=idx, en1=idx+hseg;
            int st2=idx+hseg, en2=min(n,idx+seg);
            //printf("idx %d merging [%d .. %d] and [%d .. %d] with hseg %d\n", idx, st1, en1, st2, en2, hseg);
            while(idx<en2){
                int a1=INF,a2=INF;
                if(st1<en1) a1=d_input[st1];
                if(st2<en2) a2=d_input[st2];
                if(a1<=a2){
                    d_temp[idx++] = a1;
                    st1++;
                } else {
                    d_temp[idx++] = a2;
                    st2++;
                }
            }
            for(idx=id; idx<en2; idx++){
                d_input[idx] = d_temp[idx];
            }
        }
        int req_counter = (n+seg-1)/seg; //ceil(n/seg)
        atomicAdd(&counter, 1);
        int guard = 1000;
        while(counter < req_counter && guard-- > 0) ;
        printf("counter from %d: %d\n", id, counter);
        counter = 0;
        hseg *= 2;
        seg *= 2;   
        //__syncthreads();
    
    }
    //d_temp[idx] = d_input[idx];
}
