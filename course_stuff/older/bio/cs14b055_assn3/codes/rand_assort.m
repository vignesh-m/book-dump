ntrials = 50;
trials = zeros(ntrials,1);
for i = 1:ntrials
    fprintf('trial %d\n',i);
    class1=class(randperm(length(class)));
    trials(i) = class_assort(g,invmap, protein, class1);
end