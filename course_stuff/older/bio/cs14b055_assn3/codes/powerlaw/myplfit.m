function [ param, plaw ] = myplfit( xdata , ydata, param0)
%pMYPLFIT Summary of this function goes here
%   Detailed explanation goes here
    plaw = @(param,data) param(1)*(data.^(param(2)));
    param = lsqcurvefit(plaw, param0, xdata, ydata);
end

