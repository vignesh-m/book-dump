function [ cc , avg, clus_deg ] = clustering_coeff( g )
%clustering_coeff Find clustering_coeff of each vertex in given graph
%   clustering_coeff(g) - returns array of clustering coeffs, corresponding
%   to vertex order, average clustering coeff, and average clustering
%   coefficient grouped by degree
    n = size(g,1);
    cc = zeros(n,1);
    for i = 1:n
        % find neighbours
        nbs = (1:n);
        nbs = nbs(g(i,:)==1);
        % get neighbour pair
        if size(nbs) ~= 0
            nbps = combnk(nbs,2);
            inds = sub2ind(size(g), nbps(:,1), nbps(:,2));
            edges = g(inds);
            if size(edges)>0
                cc(i) = mean(g(inds));
            else
                cc(i)=0;
            end
        end
    end
    avg = mean(cc);
    [dd, degs] = degree_dist(g);
    ccs = zeros(n+1,1);
    for i=1:n
        ccs(degs(i)+1)=ccs(degs(i)+1)+cc(i);
    end
    clus_deg = ccs./dd;
    clus_deg(isnan(clus_deg))=0;
end

