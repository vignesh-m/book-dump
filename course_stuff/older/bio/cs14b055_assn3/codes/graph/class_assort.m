function [ assort ] = class_assort( g, id_to_name, name_list, class_list )
    name_to_class = containers.Map(name_list, class_list);
    [r,c,~]=find(g);
    same = 0;
    all = 0;
    for k = 1:size(r)
        if mod(k, 100000) == 0
            fprintf('at edge %d\n',k)
        end
        i = r(k);
        j = c(k);
        all = all+1;
        same = same + strcmp(name_to_class(id_to_name(i)), name_to_class(id_to_name(j)));
    end
    assort = same/all;
end

