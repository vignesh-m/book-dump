function [ dd, ds ] = degree_dist( g )
%DEGREE_DIST Find degree distribution of given graph (as 2-d adj matrix)
%   DEGREE_DIST(g) = array of size n with i'th index = number 
%   of vertices of degree i-1
%   Returns degree dist as frequency vs degree and just list of degrees
%   To plot do plot(0:n-1,dd)
    ds=full(sum(g, 2));
    n=size(g, 1);
    dd=histc(ds, 0:n);
end

