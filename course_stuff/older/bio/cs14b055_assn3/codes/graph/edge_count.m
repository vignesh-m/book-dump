function [ ec, den ] = edge_count ( g )
%edge_count Count number of edges in a graph
%   edge_count(g) gives number of edges and density
    ec = full(sum(sum(g))/2);
    n = size(g,1);
    maxe = n*(n-1)/2;
    den = ec/maxe;
end

