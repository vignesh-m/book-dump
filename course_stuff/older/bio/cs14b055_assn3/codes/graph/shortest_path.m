function [ dist, avg, dia ] = shortest_path( g )
%shortest_path Find shortest path between all pair of vertices
%   shortest_path(g) returns 2d array with entries as
%   shortest path lengths
    n = size(g,1);
    dist = distances(graph(g));
%     dist(dist==0) = n+1;
%     dist(logical(eye(n)))=0;
%     for k=1:n
%         d2 = repmat(dist(k,:),n,1) + repmat(dist(:,k),1,n);
%         dist = min(dist, d2);
% %         for i=1:n
% %             for j=1:n
% %                 dist(i,j)=min(dist(i,j),dist(i,k)+dist(k,j));
% %             end
% %         end
%     end
    nzd = dist(dist>=1 & dist<=n);
    avg = full(mean(nzd));
    dia = full(max(nzd));
end

