function [ deg_edge, avg_deg, r ] = assortativity( g )
%assortativity Finds assortativity with respect to degree
%   assortativity(g) returns deg_edge array where i,j th entry = fraction
%   of edges between degree i and degree j vertices, avg_deg = average
%   degree of neighbours of vertex with given degree, r = assortativity
%   coefficient
    n = size(g,1);
    [~, deg] = degree_dist(g);
    maxdeg = max(deg);
    deg_edge=zeros(maxdeg+1);
    sz = maxdeg+1;
    [r,c,~] = find(g);
    for k=1:size(r)
        if mod(k, 100000) == 0
            sprintf('at edge %d',k)
        end
        i = r(k);
        j = c(k);
        deg_edge(deg(i)+1,deg(j)+1) = deg_edge(deg(i)+1,deg(j)+1) + 1;
    end
%     deg_edge(logical(eye(n+1)))=diag(deg_edge)/2;
    deg_edge=deg_edge/sum(sum(deg_edge));
    % e2[k][k'] = P(k'|k)
    q=sum(deg_edge,2);
    e2 = deg_edge./repmat(q,1,sz);
    e2(isnan(e2))=0;
    % avg_deg[k] = sum {over k'} {k'P(k'|k)}
    avg_deg = e2.*repmat(0:sz-1,sz,1);
    avg_deg = sum(avg_deg,2);
    
%     [r,c,v]=find(avg_deg);
%     plot(r,v)
%     polyfit(r,v,1)
    % assortativity index
    % source : http://www.sci.unich.it/~francesc/teaching/network/assortative.html
    se=sum(sum(g.*repmat(deg,1,n).*repmat(deg',n,1)));
    s1=sum(deg);
    s2=sum(deg.^2);
    s3=sum(deg.^3);
    r= (s1*se-s2^2)/(s1*s3-s2^2);
    
end

