function [ g,idMap ,invMap] = edge_list_to_graph( e1, e2 )
%edge_list_to_graph Convert list of edges to graph
%   edge_list_to_graph(e1, e2) e1, e2 are lists of vertex identifiers.
%   edge between e1(i) and e2(i)
    ids = unique([e1; e2]);
    n = size(ids,1);
    idMap = containers.Map(ids, 1:n);
    invMap = containers.Map(1:n, ids);
    g = sparse(zeros(n));
    sprintf('number of vertices %d',n)

    for i=1:size(e1)
%         e1(i)
%         e2(i)
%         idMap(e1(i))
%         idMap(e2(i))
        if mod(i, 100000) == 0
            sprintf('at edge %d',i)
        end
        g(idMap(e1{i}),idMap(e2{i}))=1;
        g(idMap(e2{i}),idMap(e1{i}))=1;
    end

end

