% g = sparse(random_graph('ba', [1000 0.025]));
% g = sparse(random_graph('er', [10000 0.0002]));
% g = sparse(random_graph('er', [1000 0.1]));

n = size(g,1);
[ec, dens] = edge_count(g);
fprintf(1,'Edge Count %d\n',ec);
fprintf(1,'Density %f\n',dens);

[cc_list, avg_cc, clus_deg] = clustering_coeff(g);
fprintf(1,'Clustering Coefficient %f\n',avg_cc);
fprintf(1,'C(k) vs k: \n');
[r,~,v] = find(clus_deg);
figure();
plot(r,v);
title('C(k) vs k');
xlabel('Degree, k');
ylabel('Clustering Coeff of degree k, C(k)');

[shp, avg_path, dia] = shortest_path(g);
fprintf(1,'Diameter %d\n',dia);
fprintf(1,'Characteristic Path Length %f\n',avg_path);

[deg_dist, degs] = degree_dist(g);
fprintf(1,'Degree Distrubution\n');
figure();
plot(0:max(degs)+1, deg_dist(1:max(degs)+2));
title('Degree Distribution');
xlabel('Degree');
ylabel('Frequency');
figure();
plot(0:10, deg_dist(1:11));
title('Degree Distribution - 0:10');
xlabel('Degree');
ylabel('Frequency');

[deg_prob, deg_adj, r_ass] = assortativity(g); 
% fprintf(1,'Assortativity plot\n');
% [r,~,v] = find(deg_adj);
% figure();
% plot(r,v);
% title('Degree Assortativity Correlation plot');
% xlabel('Degree');
% ylabel('Average Neighbour degree');
% fprintf(1,'Assortativity coeff %f\n', r_ass);


% figure();
% nb = max(degs)+2;
% x = 0:n;
% y = n.*binopdf(x,n,0.1);
% plot(0:max(degs)+1, [deg_dist(1:max(degs)+2)'; y(1:nb)])
% title('Degree Distribution');
% xlabel('Degree');
% ylabel('Frequency');
% legend('Degree Distribution', 'Binomial Distribution','Location','northwest');
% 
% 
% [phat, pci]=binofit(degs, 1000);

% ns=[100:100:1000 1000:1000:10000];
% ls=zeros(size(ns));
% ds=zeros(size(ns));
% for i=1:size(ns,2)
%    g=sparse(random_graph('ba', [ns(i) 0.01]));
%    [~,l,d]=shortest_path(g);
%    ls(i)=l;
%    ds(i)=d;
% end

