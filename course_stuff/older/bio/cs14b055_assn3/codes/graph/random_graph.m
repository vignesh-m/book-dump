function [ g ] = random_graph( type, args )
%random_graph Construct random graphs based on various models
%   random_graph(type, args) - type is the model, args is the parameters of
%   the model.
%   random_graph('er',[n p]) creates a graph with n vertices 
%   with probability of each edge existing p. This is the Erdos-Renyi
%   model.
%   random_graph()
    if type == 'er'
        % ER model
        n = args(1);
        p = args(2);
        g = double(rand(n,n)<p);
        % make undirected
        % add upper triangle (above diagonal) transpose to its converse  -
        % effectively makes lower triangle equal to upper triangle - we get 
        % symmetric graph with p fraction 1 and diagonals 0
        % Source: http://stackoverflow.com/q/9750432
        g = triu(g,1)+triu(g,1)' ;
    elseif type == 'ba'
        % BA model
        n = args(1);
        g = zeros(n);
        g(1,2)=1;g(2,1)=1;
        degs = zeros(n,1);
        degs(1)=1;degs(2)=1;
        for i = 1:(n-2)
            % i+1 vertices present, add 1 more
            probs = degs/sum(degs);
            % scale so that avg edges = p*(n-1)
%             probs = probs * p * (i+1);
            edges = double(probs(1:i+1)' > rand(1,i+1));
            g(i+2,1:i+1) = edges;
            g(1:i+1,i+2) = edges';
            degs(i+2) = sum(edges);
            degs(1:i+1) = degs(1:i+1)+edges';
        end
    else
        disp('unknown graph type');
        n = args(1);
        g = zeros(n);
    end
end

