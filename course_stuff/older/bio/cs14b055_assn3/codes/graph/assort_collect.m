tr=1000;
rc=zeros(tr,1);
for i=1:tr
    g = sparse(random_graph('er', [1000 0.002]));
    [~,~,r]=assortativity(g);
    rc(i)=r;
end
rc